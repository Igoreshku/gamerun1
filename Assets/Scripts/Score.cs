using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Score : MonoBehaviour
{
    [SerializeField] private Transform player;
    public Text ScoreText;
    public uint Multiply = 25;
    private uint score = 1;
    
    

    private void FixedUpdate()
    {
        StartCoroutine(ScoreRun());
    }
    private IEnumerator ScoreRun()
    {
        ScoreText.text = score.ToString();
        yield return new WaitForSeconds(3);
        score += Multiply / 25 ;
    }
}
