using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject pauseImage;
    public AudioSource music;
    private bool pause;
    private bool musicenable;

    public void PauseOnOff(GameObject panel)
    {
        if (!panel.activeSelf)
        {
            Time.timeScale = 0;
            MainMenu.OpenClose(panel);
        }
        else
        {
            Time.timeScale = 1;
            MainMenu.OpenClose(panel);
        }
    }

    public void ToMenu()
    {
        Loze.ToMenu();
    }

    public void MusicOnOff()
    {
        if (musicenable)
        {
            music.enabled = true;
            musicenable = false;
        }
        else
        {
            music.enabled = false;
            musicenable = true;
        }
    }
}
