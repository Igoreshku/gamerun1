using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public GameObject StoreImage;

    private void Start()
    {
        int coins = PlayerPrefs.GetInt("Coins");
    }

    public void StoreOnOff(GameObject panel)
    {
        MainMenu.OpenClose(panel);
    }

    public void Buy(int price)
    {
        int coins = PlayerPrefs.GetInt("Coins");
        if (coins >= price)
        {
            
            coins -= price;

            PlayerPrefs.SetInt("Coins",coins);
            
        }
    }
}
