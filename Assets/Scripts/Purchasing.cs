using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class Purchasing : MonoBehaviour
{
    public void OnPurchaceComplete(Product product)
    {
        switch (product.definition.id)
        {
            case "com.game.runner.500coins":
                break;
        }
    }

    public void Add500Coins()
    {
        int coins = PlayerPrefs.GetInt("Coins");
        coins += 500;
        PlayerPrefs.SetInt("Coins", coins);
    }
}
