using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;

public class MainMenu : MonoBehaviour
{
    public GameObject[] loading;
    [SerializeField] private GameObject loadcanvas;
    [SerializeField] private Text coin;
    
    
    private void FixedUpdate()
    {
        int coins = PlayerPrefs.GetInt("Coins");
        coin.text = coins.ToString();
    }
    public void Playing()
    {
        loadcanvas.SetActive(true);
        for (int i = 0; i < 2; i++)
        {
            StartCoroutine(Loadingscene(i));
            
        }
        SceneManager.LoadScene(1);
    }
    private IEnumerator Loadingscene(int number)
    {
        
        yield return new WaitForSeconds(2f);
        loading[number].SetActive(true);
        
    }  

    public static void OpenClose(GameObject panel)
    {
        bool IsActive = panel.activeSelf;
        panel.SetActive(!IsActive);
    }
}
