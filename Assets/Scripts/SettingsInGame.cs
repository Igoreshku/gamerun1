using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsInGame : MonoBehaviour
{
    public AudioSource music;
    private bool musicenable = true;
    public GameObject settings;

    public void OnOffMusic()
    {
        if (musicenable)
        {
            music.enabled = true;
            musicenable = false;
        }
        else
        {
            music.enabled = false;
            musicenable = true;
        }
    }

    public void SettingsOnOff(GameObject panel)
    {
        MainMenu.OpenClose(panel);
    }

    public void VKURL()
    {
        Application.OpenURL("https://vk.com/prostto_dodik");
    }
}
