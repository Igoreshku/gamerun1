using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Loze : MonoBehaviour
{
    public GameObject[] loading;
    [SerializeField] private GameObject loadcanvas;
    [SerializeField] Text recordscore;
    
    private void Start()
    {
        int lastrunscore = PlayerPrefs.GetInt("ScoreMax");
        int recordrunscore = PlayerPrefs.GetInt("RecordScore");
        if(lastrunscore > recordrunscore)
        {
            recordrunscore = lastrunscore;
            PlayerPrefs.SetInt("RecordScore", recordrunscore);
            recordscore.text = recordrunscore.ToString();
        }
        else
        {
            recordscore.text = recordrunscore.ToString();
        }
    }
    public void Restart()
    {
        loadcanvas.SetActive(true);
        for (int i = 0; i < 2; i++)
        {
            StartCoroutine(Loadingscene(i));

        }
        SceneManager.LoadScene(1);
    }
    public static void ToMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
    private IEnumerator Loadingscene(int number)
    {
        loading[number].SetActive(true);
        yield return new WaitForSeconds(2f);

    }
}
